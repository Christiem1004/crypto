#include "../include/CryptoImpl.h"

#include <string>

using namespace std;

CryptoImpl::CryptoImpl() {

}

CryptoImpl::~CryptoImpl() {

}

// Override the pure virtual functions from the Crypto class.

/**
 * "encodes" by returning the input text
 *
 * @param s - the input string
 * @return the same string
 */
string CryptoImpl::encode(string s) {
    return s+s;
}

/**
 * "decodes" by returning the input text
 *
 * @param s - the input string
 * @return the same string
 */
string CryptoImpl::decode(string s) {
    return s;
}
